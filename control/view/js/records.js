
(function render(){
	$.ajax({
		url: "http://localhost/control/controller/listpayment.php",
		type: "POST",
		dataType:"json",
		success: function(r){
			r.forEach((v)=>{
				let date = v.date_now.split("-");
				$("#list").append('\
					    <tr>\
					      <td class="mdl-data-table__cell--non-numeric">'+v.nome+'</td>\
					      <td>'+v.matricula+'</td>\
					      <td>'+v.turno+'</td>\
					      <td>'+date.join("/")+'</td>\
					      <td><i onclick="printById('+v.id+')" class="material-icons">&#xE8AD;</i></td>\
					    </tr>\
					    ');
			});

			console.log(r);
		},
		error : function(e){
			console.log(e);
		}
	});
})()

function printById(id){
	window.location = "http://localhost/control/view/html/model.html?id=" + id;
}