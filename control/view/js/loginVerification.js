var isAdm;
(function loginVerification(){
	if(JSON.stringify(findGetParameter("id_adm"))){
		$.ajax({
			url: "http://localhost/control/controller/loginVerification.php",
			type: "POST",
			dataType:"json",
			data : {id_adm : JSON.stringify(findGetParameter("id_adm"))},
			success: function(r){
				console.log(r);
				isAdm = r;
				if(r.responseText == "sai")
					kick();
			},
			error : function(e){
				console.log(e);
				if(e.responseText == "sai")
					kick();
			}
		});
	}else{
		kick();
	}
})()
verifyLicence();
function verifyLicence(){
		$.ajax({
			url: "http://localhost/control/controller/license.php",
			type: "POST",
			dataType:"json",
			success: function(r){

				console.log(r);
			},
			error : function(e){
				let today = getToday(),expire = e.responseText;
				if( (process(today).getTime() > process(expire).getTime()))
				{
				    alert("Sua licença expirou contate os desenvolvedores :D.");
				    kick();	
				}else{
					console.log("sua licença vai até " + expire)
				}
				//console.log(e);
			}
		});
}


function kick(){
	location.href = "login.html";
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

function process(date){
   var parts = date.split("/");
   return new Date(parts[2], parts[1] - 1, parts[0]);
}

function getToday(){
	let today = new Date();
	let dd = today.getDate();
	let mm = today.getMonth()+1; //January is 0!

	let yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	return dd+'/'+mm+'/'+yyyy;
}