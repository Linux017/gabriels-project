var verification = {
	loginForm: (usuario,senha) => {
		if(usuario=="" && senha==""){
			verification.message = "Preencha todos os campos.";
			return false;
		}else if(usuario=="" || usuario==null){
			verification.message = "Preencha o campo de usuário.";
			return false;
		}else if(senha=="" || senha==null){
			verification.message = "Preencha o campo de senha.";
			return false;
		}else{
			return true;
		}
	},
	configForm : (price,since,until) =>{
		if(price==""&& since==""&& until==""){
			verification.message = "Preencha todos os campos.";
			return false;
		}else if(since=="" || since==null){
			verification.message = "Preencha o campo de data inicial.";
			return false;
		}
		else if(until=="" || until==null){
			verification.message = "Preencha o campo de data final.";
			return false;
		}
		else if(price=="" || price==null){
			verification.message = "Preencha o campo de preço.";
			return false;
		}else{
			return true;
		}
	},
	mainForm : (name,matri,shift) =>{
		if(name=="" && matri=="" && shift=="" && price==""&& since==""&& until==""){
			verification.message = "Preencha todos os campos.";
			return false;
		}else if(name=="" || name==null){
			verification.message = "Preencha o campo de Nome.";
			return false;
		}else if(matri=="" || matri==null){
			verification.message = "Preencha o campo de matrícula.";
			return false;
		}else if(shift=="" || shift==null){
			verification.message = "Preencha o campo de turno.";
			return false;
		}else if(since=="" || since==null){
			verification.message = "Preencha o campo de data inicial.";
			return false;
		}
		else if(until=="" || until==null){
			verification.message = "Preencha o campo de data final.";
			return false;
		}
		else if(price=="" || price==null){
			verification.message = "Preencha o campo de preço.";
			return false;
		}else{
			return true;
		}
	},message : ''
};