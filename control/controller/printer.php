<?php
	include "fpdf/fpdf.php";
	header("Content-Type: application/json");
	
	if(isset($_GET['data'])){
		$data = (object) json_decode($_GET['data']);
		$pdf = new FPDF();
		$pdf->AddPage("L");
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(10,20,$data->{'name'});
		$pdf->Text(10,40,$data->{'id'});
		$pdf->Text(10,60,$data->{'shift'});
		$pdf->Text(10,80,$data->{'since'});
		$pdf->Text(10,100,$data->{'until'});
		$pdf->Text(10,120,$data->{'price'});
		$pdf->Cell(40,10,'');
		$pdf->Output();
	}else{
		echo '<link rel="stylesheet" type="text/css" href="../view/css/error.css">';
		echo "<div><img src='../view/img/sad.png'><br><h1>Algo deu errado volte e tente novamente.</h1></div>";
	}
?>
