-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2018 at 04:44 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `control`
--

-- --------------------------------------------------------

--
-- Table structure for table `license`
--

CREATE TABLE `license` (
  `expire` text NOT NULL,
  `now` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `license`
--

INSERT INTO `license` (`expire`, `now`) VALUES
('03/08/2018', '22/04/2018');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `usuario` text NOT NULL,
  `senha` text NOT NULL,
  `cat` int(11) NOT NULL,
  `id_adm` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`usuario`, `senha`, `cat`, `id_adm`) VALUES
('ADM', '0cc175b9c0f1b6a831c399e269772661', 0, 3),
('bot', '0cc175b9c0f1b6a831c399e269772661', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `padrao`
--

CREATE TABLE `padrao` (
  `de` text NOT NULL,
  `ate` text NOT NULL,
  `valor` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `padrao`
--

INSERT INTO `padrao` (`de`, `ate`, `valor`, `id`) VALUES
('22/02/2018', '22/05/2018', '90', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pagamento`
--

CREATE TABLE `pagamento` (
  `id` int(11) NOT NULL,
  `nome` text COLLATE utf8_bin,
  `matricula` text COLLATE utf8_bin,
  `turno` text COLLATE utf8_bin,
  `date_now` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pagamento`
--

INSERT INTO `pagamento` (`id`, `nome`, `matricula`, `turno`, `date_now`) VALUES
(1, 'JAILSON', 'd21', '321', '24/04/2018'),
(18, '2w12', '2w1w', 'w21', '24-04-2018'),
(19, 'Lino Cavalcante Mota Neto', '232', 'Diurno', '24-04-2018'),
(20, 'Lino Cavalcante Mota Neto', '24', 'Tarde', '24-04-2018');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_adm`);

--
-- Indexes for table `pagamento`
--
ALTER TABLE `pagamento`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_adm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pagamento`
--
ALTER TABLE `pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
